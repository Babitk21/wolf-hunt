using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Rendering;
using static UnityEditor.Searcher.SearcherWindow.Alignment;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] FloatingJoystick joystick;
    [SerializeField] float moveSpeed, jumpSpeed;
    [SerializeField] Transform enemy, virCamera;
    [SerializeField] List<GameObject> speedParticles;

    Rigidbody rb;
    bool isHunting, canMove = true;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = joystick.Horizontal;
        float verticalInput = joystick.Vertical;
        Vector3 movementDir = new Vector3(horizontalInput, 0, verticalInput);
        movementDir.Normalize();
        var rotation = Quaternion.LookRotation(movementDir);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 10);
        if (canMove)
        {
            Movement(movementDir);
        }

        Vector3 rot = virCamera.eulerAngles;
        rot.y += horizontalInput;
        //virCamera.eulerAngles = rot;    

        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (var particle in speedParticles)
            {
                particle.SetActive(true);
            }
            animator.SetTrigger("jump");
            rb.AddForce(new Vector3(0, jumpSpeed, 0), ForceMode.Impulse);
            isHunting = true;
            canMove = false;
        }
        if(isHunting)
        {
            IsHunting();
        }
    }
    void Movement(Vector3 moveDir)
    {
        transform.position += moveDir * moveSpeed * Time.deltaTime;
        if(moveDir != Vector3.zero)
        {
            animator.SetInteger("Walk", 1);
        }
        else
        {
            animator.SetInteger("Walk", 0);
        }
    }
    void IsHunting()
    {
        transform.LookAt(enemy);

        Vector3 dir = enemy.position - transform.position;
        float distance = dir.magnitude;
        if(distance > 0.1f)
        {
            Vector3 movement = dir.normalized * Time.deltaTime * moveSpeed * 4f;
            transform.Translate(movement, Space.World);
        }



    }
}
