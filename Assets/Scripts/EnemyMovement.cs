using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    NavMeshAgent agent;
    private Vector3 destination;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        SetNewDestination();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
            SetNewDestination();
    }

    private void SetNewDestination()
    {
        Vector3 randomDirection = Random.insideUnitSphere * 10f; // Adjust the radius as needed
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, 10f, NavMesh.AllAreas);
        destination = hit.position;
        agent.SetDestination(destination);
    }

}
